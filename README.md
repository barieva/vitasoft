### Vita-soft
For further reference, please consider the following sections:

1) Order and print strings in ascending order of their length values. If they match, arrange them in lexicographic order.
2) Return a string with the name of the month.
   Check the correctness of the number input.

___
Bariev Abdul  
getJavaJob