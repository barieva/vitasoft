package com.vitasoft.vitasoft.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ContextConfiguration(classes = SentencesSorterService.class)
@ExtendWith(SpringExtension.class)
class SentencesSorterServiceTest {
    @Autowired
    SentencesSorterService sentencesSorterService;

    @Test
    void sortSequencesWithDifferentLengths() {
        List<String> nonSortedSequencesWithDifferentLength = asList("Рог золотой выплывает луны.",
                "Тихо струится река серебристая",
                "Солнце садится за горы лесистые.",
                "В царстве вечернем зеленой весн");
        List<String> sortedSequences = asList("(27): Рог золотой выплывает луны.",
                "(30): Тихо струится река серебристая",
                "(31): В царстве вечернем зеленой весн",
                "(32): Солнце садится за горы лесистые.");
        assertEquals(sortedSequences, sentencesSorterService.getSortedSentencesWithTheirLengths(nonSortedSequencesWithDifferentLength));
    }

    @Test
    void sortSequencesWithSameLengths() {
        List<String> nonSortedSequencesWithSameLength = asList("В царстве вечернем зеленой весн2",
                "В царстве вечернем зеленой весн1");
        List<String> sortedSequences = asList("(32): В царстве вечернем зеленой весн1",
                "(32): В царстве вечернем зеленой весн2");
        assertEquals(sortedSequences, sentencesSorterService.getSortedSentencesWithTheirLengths(nonSortedSequencesWithSameLength));
    }

    @Test
    void getSortedSentencesWithTheirLengthsFromNullValue() {
        assertThrows(IllegalArgumentException.class,
                () -> sentencesSorterService.getSortedSentencesWithTheirLengths(null));
    }

    @Test
    void sortEmptyList() {
        assertEquals(new ArrayList<>(), sentencesSorterService.getSortedSentencesWithTheirLengths(new ArrayList<>()));
    }
}