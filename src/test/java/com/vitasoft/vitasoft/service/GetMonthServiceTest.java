package com.vitasoft.vitasoft.service;

import com.vitasoft.vitasoft.common.Month;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration(classes = GetMonthService.class)
@ExtendWith(SpringExtension.class)
class GetMonthServiceTest {

    @Autowired
    GetMonthService getMonthService;

    @Test
    void getMonthName() {
        String january = getMonthService.getMonthName(1);
        assertEquals(Month.JANUARY.getMonthName(), january);
    }

    @Test
    void getMonthNameByLessThanOneNumber() {
        String errorMessage = getMonthService.getMonthName(0);
        assertEquals("INCORRECT_INPUT_DATA", errorMessage);
    }

    @Test
    void getMonthNameByMoreThanTwelveNumber() {
        String errorMessage = getMonthService.getMonthName(13);
        assertEquals("INCORRECT_INPUT_DATA", errorMessage);
    }
}