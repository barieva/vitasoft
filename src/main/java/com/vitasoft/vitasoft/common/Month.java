package com.vitasoft.vitasoft.common;

public enum Month {
    JANUARY(1, "Я-Н-В-А-Р-Ь"),
    FEBRUARY(2, "Ф-Е-В-Р-А-Л-Ь"),
    MARCH(3, "М-А-Р-Т"),
    APRIL(4, "А-П-Р-Е-Л-Ь"),
    MAY(5, "М-А-Й"),
    JUNE(6, "И-Ю-Н-Ь"),
    JULY(7, "И-Ю-Л-Ь"),
    AUGUST(8, "А-В-Г-У-С-Т"),
    SEPTEMBER(9, "С-Е-Н-Т-Я-Б-Р-Ь"),
    OCTOBER(10, "О-К-Т-Я-Б-Р-Ь"),
    NOVEMBER(11, "Н-О-Я-Б-Р-Ь"),
    DECEMBER(12, "Д-Е-К-А-Б-Р-Ь");

    int monthNumber;
    String monthName;

    Month(int monthNumber, String monthName) {
        this.monthNumber = monthNumber;
        this.monthName = monthName;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public String getMonthName() {
        return monthName;
    }
}