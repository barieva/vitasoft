package com.vitasoft.vitasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VitasoftApplication {
    public static void main(String[] args) {
        SpringApplication.run(VitasoftApplication.class, args);
    }

}
