package com.vitasoft.vitasoft.service;

import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class SentencesSorterService {
    public List<String> getSortedSentencesWithTheirLengths(List<String> sentences) {
        if (sentences != null) {
            return sentences.stream().sorted(getComparator())
                    .map(sentence -> sentence = getSentenceWithItsLength(sentence))
                    .collect(toList());
        }
        throw new IllegalArgumentException("No value is present");
    }

    private String getSentenceWithItsLength(String sentence) {
        return "(" + sentence.length() + "): " + sentence;
    }

    private Comparator<String> getComparator() {
        return (sentenceOne, sentenceTwo) -> {
            if (sentenceOne.length() > sentenceTwo.length()) {
                return 1;
            } else if (sentenceOne.length() == sentenceTwo.length()) {
                return sentenceOne.compareTo(sentenceTwo);
            } else {
                return -1;
            }
        };
    }
}