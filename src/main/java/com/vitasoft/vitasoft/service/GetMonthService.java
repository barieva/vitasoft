package com.vitasoft.vitasoft.service;

import com.vitasoft.vitasoft.common.Month;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Arrays.stream;

@Service
public class GetMonthService {
    private static final String INCORRECT_INPUT_DATA = "INCORRECT_INPUT_DATA";

    public String getMonthName(int monthNumber) {
        if (isMonthNumber(monthNumber)) {
            return getName(monthNumber);
        }
        return INCORRECT_INPUT_DATA;
    }

    private String getName(int monthNumber) {
        return getMonth(monthNumber).get().getMonthName();
    }

    private Optional<Month> getMonth(int monthNumber) {
        return stream(Month.values()).filter(month -> month.getMonthNumber() == monthNumber).findFirst();
    }

    private boolean isMonthNumber(int monthNumber) {
        return monthNumber >= 1 && monthNumber < 12;
    }
}