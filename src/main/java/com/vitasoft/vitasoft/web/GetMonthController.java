package com.vitasoft.vitasoft.web;

import com.vitasoft.vitasoft.service.GetMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetMonthController {
    private GetMonthService getMonthService;

    @Autowired
    public void setGetMonthService(GetMonthService getMonthService) {
        this.getMonthService = getMonthService;
    }

    @PostMapping("/month/{monthNumber}")
    public String getMonth(@PathVariable("monthNumber") int monthNumber) {
        return getMonthService.getMonthName(monthNumber);
    }
}