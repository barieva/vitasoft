package com.vitasoft.vitasoft.web;

import com.vitasoft.vitasoft.service.SentencesSorterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SentencesSorterController {
    private SentencesSorterService sentencesSorterService;

    @Autowired
    public void setSentencesSorterService(SentencesSorterService sentencesSorterService) {
        this.sentencesSorterService = sentencesSorterService;
    }

    @PostMapping("/sort")
    public List<String> sort(@RequestBody List<String> sentences) {
        return sentencesSorterService.getSortedSentencesWithTheirLengths(sentences);
    }
}